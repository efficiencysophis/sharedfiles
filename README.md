## [29.12.2021 by NAV] Creation.

This repository will include files from ThirdParty providers.

Usually we got them from NuGet package manager, so by clone this repository you will have all you need,
to build your source code correctly from scratch on any PC.
Added:
+ log4net.2.0.13
+ Newtonsoft.Json.13.0.1
+ Oracle.ManagedDataAccess.19.13.0
+ RestSharp.106.12.0
